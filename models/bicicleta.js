const { coerce } = require('debug');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: String,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: '2dsphere', sparse: true } //es un indice de ubicacion
    }
});

bicicletaSchema.methods.toString = function () {
    return 'id: ' + this.code + ' | color: ' + this.color;
}

bicicletaSchema.statics.createInstance = function (code, color, modelo, ubicacion) {
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};

bicicletaSchema.statics.allBicis = function (cb) {
    return this.find({}, cb); //le paso el filtro vacio xq quiero todas las bicis - cb es call back
}

bicicletaSchema.statics.add = function (aBici, cb) {
    this.create(aBici, cb);
}

bicicletaSchema.statics.findByCode = function (aCode, cb) {
    return this.findOne({ code: aCode }, cb);
}

bicicletaSchema.statics.removeByCode = function (aCode, cb) {
    return this.deleteOne({ code: aCode }, cb); //{} => criterio de filtrado
}

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

/*var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}


Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici) {
        return aBici
    } else {
        throw new Error (`No existe la bici con el id: ${aBiciId}`)
    }
}

Bicicleta.removeId = function(aBiciId){

    //var aBici = Bicicleta.allBicis.findById(x => x.id == aBiciId);
    //Bicicleta.allBicis.splice(aBici, 1)


    for (var i=0; i < Bicicleta.allBicis.length; i++){
        if (Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i, 1);
            break;
        }
    }
}

var a = new Bicicleta (1, 'rojo', 'urbana', [-34.601242, -58.3861])

var b = new Bicicleta (2, 'blanca', 'urbana', [-34.5962, -58.3761])

var c = new Bicicleta (3, 'gris', 'urbana', [-34.5862, -58.3661])

var d = new Bicicleta (4, 'azul', 'urbana', [-34.6162, -58.3781])

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);
Bicicleta.add(d);


module.exports = Bicicleta;
*/